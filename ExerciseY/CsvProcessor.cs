﻿using System.Collections.Generic;
using System.Linq;
using ExerciseY.Model;

namespace ExerciseY
{
    public interface ICsvProcessor
    {
        IEnumerable<NameFrequency> GetNameData();
        IEnumerable<Address> GetAddressData();
    }

    public class CsvProcessor : ICsvProcessor
    {
        private readonly ICsvDao _dao;

        public CsvProcessor(ICsvDao dao)
        {
            _dao = dao;
        }

        public IEnumerable<Address> GetAddressData()
        {
            return _dao.GetData().Select(i => new Address(i.Address))
                .OrderBy(i => i.Street());
        }
        
        public IEnumerable<NameFrequency> GetNameData()
        {
            return _dao.GetData().Select(GetValue)
                .SelectMany(i => i) //Now have all names first and last
                .GroupBy(i => i)
                .Select(i => new NameFrequency(i.Key, i.Count())) //Project into new Model object with count of grouping
                .OrderBy(i => i.Count) 
                .ThenBy(i => i.Name);
        }

        private static IEnumerable<string> GetValue(DataRow row)
        {
            yield return row.FirstName;
            yield return row.LastName;
        }
    }
}