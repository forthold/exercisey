﻿using System;
using System.Collections.Generic;
using System.IO;
using ExerciseY.Model;
using LINQtoCSV;

namespace ExerciseY
{
    internal class Program
    {
        static Program()
        {
            CsvContext = new CsvContext();
        }

        public static CsvContext CsvContext { get; set; }

        private static int Main(string[] args)
        {
            FileInfo dataFile;
            string inputFile = args[0];
            string dataFileDirectory;

            if (args.Length != 1)
            {
                Console.Out.Write("Usage: Execute program with path to csv file as the only argument");
                return 1;
            }
            try
            {
                dataFile = new FileInfo(inputFile);
                dataFileDirectory = Path.GetDirectoryName(inputFile);
            }
            catch (Exception)
            {
                Console.Out.Write("Invalid filepath passed in,  Please correct");
                return 1;
            }

            Console.Out.WriteLine("Reading Data from {0}", inputFile);

            ICsvDao dao = new CsvDao(CsvContext, dataFile);
            ICsvProcessor processor = new CsvProcessor(dao);

            if (dataFileDirectory != null)
            {
                var namesOutputFilePath = Path.Combine(dataFileDirectory, "OrderedNamesAndFrequency.txt");
                Console.Out.WriteLine("Writing Data to {0}", namesOutputFilePath);
                IEnumerable<NameFrequency> nameData = processor.GetNameData();
                dao.WriteNameData(nameData, namesOutputFilePath);

                var addressOutputFilePath = Path.Combine(dataFileDirectory, "OrderedAddresses.txt");
                Console.Out.WriteLine("Writing Data to {0}", addressOutputFilePath);
                IEnumerable<Address> addressData = processor.GetAddressData();
                dao.WriteAddressData(addressData, addressOutputFilePath);
            }

            Console.Out.WriteLine("Press any key to exit");
            Console.ReadLine();
            return 0;
        }
    }
}