﻿using System.Collections.Generic;
using System.IO;
using ExerciseY.Model;
using LINQtoCSV;
using DataRow = ExerciseY.Model.DataRow;

namespace ExerciseY
{
    public interface ICsvDao
    {
        IEnumerable<DataRow> GetData();
        void WriteNameData(IEnumerable<NameFrequency> nameData, string outputFilename);
        void WriteAddressData(IEnumerable<Address> addressData, string outputFilename);
    }

    public class CsvDao : ICsvDao
    {
        private readonly CsvContext _ctx;

        private readonly CsvFileDescription _defaultFileDescription = new CsvFileDescription
        {
            SeparatorChar = '\t', // tab delimited
            FirstLineHasColumnNames = false, // no column names in first record
            EnforceCsvColumnAttribute = true //needed for above setting
        };

        private readonly FileInfo _file;

        public CsvDao(CsvContext ctx, FileInfo file)
        {
            _ctx = ctx;
            _file = file;
        }

        public IEnumerable<DataRow> GetData()
        {
            var inputFileDescription = new CsvFileDescription
            {
                SeparatorChar = ',',
                FirstLineHasColumnNames = true
            };

            return _ctx.Read<DataRow>(new StreamReader(_file.OpenRead()), inputFileDescription);
        }

        public void WriteNameData(IEnumerable<NameFrequency> nameData, string outputFilename)
        {
            _ctx.Write(nameData, outputFilename, _defaultFileDescription);
        }

        public void WriteAddressData(IEnumerable<Address> addressData, string outputFilename)
        {
            _ctx.Write(addressData, outputFilename, _defaultFileDescription);
        }
    }
}