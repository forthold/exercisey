﻿RUN

To run in VS just change the path in the ExceciseY projects Debug "Command Line arguments" to the location of the project on disk. The data.csv is in the projects Resources folder.

PROBLEM DECOMPOSITION
I started by just splitting the activity into a few basic components:
1. Program structure i.e Console app, GUI, etc
2. Reading the file
3. Processing the data in accordance to requirements
4. Writing the new files

Stage 1: 
I decided to deliver the solution as a console app that takes the data files path in as a parameter. 
In addition I decided to deliver this assessment via Bitbucket (Git) so the process of coding can be seen through the commits.

Stage 2:

I decided to use a library I have used before LinqToCSV to read and write the files. 
This allowed the exercise to be written similarly to many other data processing code i.e using a context to read and write data (also lending itself to dependency injection and integration testing).

Stage 3: 
I decomposed the processing of the data into a series of Linq function calls that will munge the data from the IEnumerable of input models (DataRows) returned by the context into lists of the two output models (Address and NameFrequency).
These are all standard Linq functions. 
I interpreted the ordering instructions for Names:
	 "The first should show the frequency of the first and last names ordered by frequency and then alphabetically"
as being a list of all the separate first and second names ordered by Count ascending and then by name in alphabetical order (seemed to look nice)

Stage 4:
This is as simple as passing these outputs collections into the context again with some metadata indicating where it is to be saved and in what format.

TESTING

As there is only a request for unit tests and not integration test some testable items are not covered such as the consequences of passing an invalid path (though I do make attempts to handle this) or testing the CSV context and generation.

I have focused on testing Stage 3 through unit tests using NUnit and Rhino Mocks.

It was irritating that LinqToCSV's main entry point CsvContext is a concrete class with no interface so difficult to mock with Rhino Mocks. This constraint and general best practice lead me to a quite standard design with a DAO doing IO and a another component holding the logic (Processor) both interfaced to aid testing, dependency injection and mocking.


