﻿using System;
using LINQtoCSV;

namespace ExerciseY.Model
{
    public class Address : IEquatable<Address>
    {
        [CsvColumn(FieldIndex = 1)]
        public string FullAddress { get; set; }

        public Address(string address)
        {
            FullAddress = address;
        }

        public string Street()
        {
            return FullAddress.Split(' ')[1];
        }

        public bool Equals(Address other)
        {
             if (FullAddress == other.FullAddress)
            {
                return true;
            }
            return false;
        }
    }
}
