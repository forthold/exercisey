﻿using LINQtoCSV;

namespace ExerciseY.Model
{
    public class DataRow
    {
        [CsvColumn(FieldIndex = 1)]
        public string FirstName { get; set; }

        [CsvColumn(FieldIndex = 2)]
        public string LastName { get; set; }

        [CsvColumn(FieldIndex = 3)]
        public string Address { get; set; }

        [CsvColumn(FieldIndex = 4)]
        public string PhoneNumber { get; set; }
    }
}