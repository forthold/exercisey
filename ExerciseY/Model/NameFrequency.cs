﻿using System;
using LINQtoCSV;

namespace ExerciseY.Model
{
    public class NameFrequency : IEquatable<NameFrequency>
    {
        public NameFrequency()
        {
        }

        public NameFrequency(string name, int count)
        {
            Name = name;
            Count = count;
        }


        [CsvColumn(FieldIndex = 1)]
        public string Name { get; set; }

        [CsvColumn(FieldIndex = 2)]
        public int Count { get; set; }

        public bool Equals(NameFrequency other)
        {
            if (this.Name == other.Name && this.Count == other.Count)
            {
                return true;
            }
            return false;
        }
    }
}
