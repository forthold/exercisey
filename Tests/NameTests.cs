﻿using System.Collections.Generic;
using System.Linq;
using ExerciseY;
using ExerciseY.Model;
using NUnit.Framework;
using Rhino.Mocks;

namespace Tests
{
    [TestFixture]
    public class NameTests
    {

        [Test]
        public void ProcessorGetNameData()
        {
            var stubCsvDao = MockRepository.GenerateStub<ICsvDao>();

            IEnumerable<DataRow> data = new List<DataRow>
            {
                new DataRow
                {
                    Address = "A1 St",
                    FirstName = "Aran",
                    LastName = "Elkington",
                    PhoneNumber = "0123456789"
                },
                new DataRow
                {
                    Address = "A2 St",
                    FirstName = "Bob",
                    LastName = "Elkington",
                    PhoneNumber = "0123456789"
                }
            };

            var expectedNameFrequency = new NameFrequency
            {
                Count = 2,
                Name = "Elkington",
            };

            stubCsvDao.Stub(x => x.GetData()).Return(data);

            var processor = new CsvProcessor(stubCsvDao);

            List<NameFrequency> nameData = processor.GetNameData().ToList();
            //Assert there are 3 unique names
            Assert.IsTrue(nameData.Count() == 3);

            //Asset that the Elkington record has a count of 2
            CollectionAssert.Contains(nameData, expectedNameFrequency);

            //Asset order by count Elkington is only one with count 2
            Assert.IsTrue(nameData.Last().Name == "Elkington");

            //Asset order by name
            Assert.IsTrue(nameData.First().Name == "Aran");
        }
    }
}
