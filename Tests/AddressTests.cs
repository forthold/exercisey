﻿using System.Collections.Generic;
using System.Linq;
using ExerciseY;
using ExerciseY.Model;
using NUnit.Framework;
using Rhino.Mocks;

namespace Tests
{
    [TestFixture]
    public class AddressTests
    {
        [Test]
        public void ProcessorGetNameData()
        {
            var stubCsvDao = MockRepository.GenerateStub<ICsvDao>();
            const string firstStreet = "4 A1 St";

            IEnumerable<DataRow> data = new List<DataRow>
            {
                new DataRow
                {
                    Address = firstStreet,
                    FirstName = "Aran",
                    LastName = "Elkington",
                    PhoneNumber = "0123456789"
                },
                new DataRow
                {
                    Address = "1 A2 St",
                    FirstName = "Bob",
                    LastName = "Elkington",
                    PhoneNumber = "0123456789"
                }
            };

            var expectedFirstAddress = new Address(firstStreet);

            stubCsvDao.Stub(x => x.GetData()).Return(data);

            var processor = new CsvProcessor(stubCsvDao);

            List<Address> addressData = processor.GetAddressData().ToList();

            //Assert there are 3 unique names
            Assert.IsTrue(addressData.Count() == 2);

            //Asset that the Elkington record has a count of 2
            CollectionAssert.Contains(addressData, expectedFirstAddress);

            //Asset order by count Elkington is only one with count 2
            Assert.IsTrue(addressData.First().Street() == "A1");
        }
    }
}